## Android 离线SDK - 正式版

1. **注意：HBuilderX 3.7.6版本开始，微信、支付宝、高德改为线上依赖，涉及到的模块有：微信支付、微信登录、微信分享、支付宝支付、高德地图、高德定位，涉及上述功能的App请参考文档重新集成。**
2. **注意：HBuilderX 3.2.5版本之后适配了AndroidX，升级时需要参考文档重新配置**

### 2023年11月27日发布——HBuilderX（3.98.2023112510）

[百度云](https://pan.baidu.com/s/14SZ-CjlbaNtGHk3CpamgXQ)，提取码: 7rfu

[和彩云](https://caiyun.139.com/m/i?115Cno1RLX28f)，提取码: 7tgK

+ 更新uni-app离线打包支持，需使用HBuilderX（3.98.2023112510）版本生成本地打包App资源。
+ 更新 uni-AD 模块 穿山甲&GroMore SDK为 5.7.0.5 版，解决加载广告可能会崩溃的Bug
+ 更新 一键登录使用的个推核心组件SDK为 3.2.9.0 版，个验SDK为 3.1.4.0 版
+ 更新 uni实人认证使用的阿里云金融级实人认证SDK为 2.3.7 版

[历史版本](https://pan.baidu.com/s/1nOAuXVH_qp4RHlouPf97fA?pwd=mayf)

[历史更新日志](/AppDocs/download/historyRelease/androidRelease.md)


## Android 离线SDK - Alpha版

### 2023年11月21日发布——HBuilderX（3.98.2023112011-alpha）

[百度云](https://pan.baidu.com/s/1NLBTW94Im_zg5R38Wiijdg) ，提取码: 5vgk

[和彩云](https://caiyun.139.com/m/i?115CeVcqseFof)，提取码: cUNU

+ 更新uni-app离线打包支持，需使用HBuilderX（3.98.2023112011-alpha）版本生成本地打包App资源。
+ 更新 uni-AD 模块 穿山甲&GroMore SDK为 5.7.0.5 版，解决加载广告可能会崩溃的Bug

[历史版本](https://pan.baidu.com/s/1BOzJygO0U39WsydCVMHomA?pwd=98nu)

[历史更新日志](/AppDocs/download/historyRelease/androidAlpha.md)
